package br.com.consumidorpositivo.challengeboavista.exception;

public class DuplicatedException extends Exception {
	private static final long serialVersionUID = 1L;

	public DuplicatedException() {
		super();
	}

	public DuplicatedException(String message) {
		super(message);
	}
}

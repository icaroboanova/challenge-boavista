package br.com.consumidorpositivo.challengeboavista;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengeBoavistaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeBoavistaApplication.class, args);
	}

}

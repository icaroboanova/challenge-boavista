package br.com.consumidorpositivo.challengeboavista.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.consumidorpositivo.challengeboavista.model.Cliente;

@Repository
public class BaseQC {
	
	@PersistenceContext
    private EntityManager manager;
	
	@SuppressWarnings("unchecked")
	public List<Cliente> listarClientes() {
		final String sql = "select c from Cliente c";
		TypedQuery<Cliente> query = (TypedQuery<Cliente>) manager.createQuery(sql);
		return query.getResultList();
	}
	
	public Cliente consultarCliente(int id) {
		return manager.find(Cliente.class, id);
	}
	
	public Cliente consultarClientePorNomeUsuario(String nomeUsuario) {
		final String sql = "select c from Cliente c where c.usuario = :usuario";
		TypedQuery<Cliente> query = (TypedQuery<Cliente>) manager.createQuery(sql).setParameter("usuario", nomeUsuario);
		if(!query.getResultList().isEmpty()) {
			return query.getResultList().get(0);
		} else {
			return null;
		}
	}
	
	@Transactional
	public void inserirCliente(Cliente cliente) {
		manager.persist(cliente);
	}
	
	@Transactional
	public void atualizarCliente(Cliente cliente) {
		manager.merge(cliente);
	}
	
	@Transactional
	public void excluirCliente(Cliente cliente) {
		manager.remove(cliente);
	}

}

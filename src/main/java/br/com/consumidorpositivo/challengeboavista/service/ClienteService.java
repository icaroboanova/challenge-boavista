package br.com.consumidorpositivo.challengeboavista.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.consumidorpositivo.challengeboavista.config.JsonConfig;
import br.com.consumidorpositivo.challengeboavista.exception.DuplicatedException;
import br.com.consumidorpositivo.challengeboavista.model.Cliente;
import br.com.consumidorpositivo.challengeboavista.persistence.BaseQC;
import br.com.consumidorpositivo.challengeboavista.util.ClienteUtil;

@Service
public class ClienteService {
	
	@Autowired
	private BaseQC baseQC;
	
	public String listarClientes() {
		Gson gson = JsonConfig.getGsonBuilder().create();
		List<Cliente> lista = baseQC.listarClientes();
		//não mostrar senhas para o front
		for(Cliente cliente: lista) {
			cliente.setSenha(null);
		}
		return gson.toJson(lista);
	}
	
	public void inserirCliente(String data) throws NoSuchAlgorithmException, UnsupportedEncodingException, DuplicatedException {
		Gson gson = JsonConfig.getGsonBuilder().create();
		Cliente cliente = gson.fromJson(data, Cliente.class);
		if(!Objects.isNull(cliente.getUsuario()) && Objects.isNull(baseQC.consultarClientePorNomeUsuario(cliente.getUsuario()))) {
			if(!Objects.isNull(cliente.getSenha())) cliente.setSenha(ClienteUtil.gerarHash(cliente.getSenha()));
			baseQC.inserirCliente(cliente);
		} else {
			throw new DuplicatedException("Nome de usuário já existente ou não informado.");
		}
	}
	
	public void atualizarCliente(int id, String data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		Gson gson = JsonConfig.getGsonBuilder().create();
		Cliente update = gson.fromJson(data, Cliente.class);
		Cliente cliente = baseQC.consultarCliente(id);
		if(!Objects.isNull(update.getNome())) cliente.setNome(update.getNome());
		if(!Objects.isNull(update.getDataNascimento())) cliente.setDataNascimento(update.getDataNascimento());
		if(!Objects.isNull(update.getTelefone())) cliente.setTelefone(update.getTelefone());
		if(!Objects.isNull(update.getEmail())) cliente.setEmail(update.getEmail());
		if(!Objects.isNull(update.getEndereco())) cliente.setEndereco(update.getEndereco());
		if(!Objects.isNull(update.getSenha())) cliente.setSenha(ClienteUtil.gerarHash(update.getSenha()));
		baseQC.atualizarCliente(cliente);
	}
	
	public void excluirCliente(int id) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		Cliente cliente = baseQC.consultarCliente(id);
		baseQC.excluirCliente(cliente);
	}

}

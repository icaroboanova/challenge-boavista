package br.com.consumidorpositivo.challengeboavista.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consumidorpositivo.challengeboavista.exception.DuplicatedException;
import br.com.consumidorpositivo.challengeboavista.service.ClienteService;

@CrossOrigin
@RequestMapping("/cliente")
@RestController
public class ClienteController {
	
	private static Logger LOG = Logger.getLogger(ClienteController.class.getName());
	
	@Autowired
	private ClienteService clienteService;
	
	/**
	 * Retorna todos os usuários cadastrados.
	 */
	@CrossOrigin
	@GetMapping("/")
	public ResponseEntity<String> listarClientes() throws Exception {
		return new ResponseEntity<String>(clienteService.listarClientes(), HttpStatus.ACCEPTED);
	}
	
	/**
	 * Insere um novo usuário na base.
	 */
	@CrossOrigin
	@PutMapping("/")
	public ResponseEntity<String> inserirCliente(@RequestBody String data) {
		try {
			clienteService.inserirCliente(data);
			return new ResponseEntity<String>("", HttpStatus.CREATED);
		} catch (DuplicatedException e ) {
			LOG.error("ERROR - inserirCliente: ", e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			LOG.error("ERROR - inserirCliente: ", e);
			return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Atualiza um usuário na base/Método deve ser usado também para troca de senha.
	 */
	@CrossOrigin
	@PostMapping("/{id}")
	public ResponseEntity<String> atualizarCliente(@PathVariable("id") int id, @RequestBody String data) {
		try {
			clienteService.atualizarCliente(id, data);
			return new ResponseEntity<String>("", HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			LOG.error("ERROR - atualizarCliente: ", e);;
			return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Remove um usuário da base.
	 */
	@CrossOrigin
	@DeleteMapping("/{id}")
	public ResponseEntity<String> excluirCliente(@PathVariable("id") int id) {
		try {
			clienteService.excluirCliente(id);
			return new ResponseEntity<String>("", HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			LOG.error("ERROR - excluirCliente: ", e);
			return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

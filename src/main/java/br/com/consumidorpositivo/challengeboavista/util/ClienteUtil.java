package br.com.consumidorpositivo.challengeboavista.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Component;

@Component
public class ClienteUtil {

	public static String gerarHash(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigestHashAdmin[] = algorithm.digest(str.getBytes("UTF-8"));
		StringBuilder hexStringHashAdmin = new StringBuilder();
		for (byte b : messageDigestHashAdmin) {
			hexStringHashAdmin.append(String.format("%02X", 0xFF & b));
		}
		return hexStringHashAdmin.toString();
	}

}

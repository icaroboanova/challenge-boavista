create database challengedb;
use challengedb;
create table clientes( id int not null auto_increment, nome varchar(50), data_nascimento date, telefone varchar(11), email varchar(50), endereco varchar(200), usuario varchar(20), senha varchar(64), primary key(id) );
SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;
SET collation_connection = utf8_general_ci;


insert into clientes values(default, 'Icaro Boa Nova Ferreira', '04/12/1997', '71983363359', 'icaroboanova@hotmail.com', 'rua cael, 50 - ap 18', 'icaroboanova', '5A797E04DD084F9E9502D0E0E54D0A0996BC9D13E14FBF1613425A8BB4B448CE');